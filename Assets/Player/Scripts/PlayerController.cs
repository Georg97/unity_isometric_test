﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Character attributes:")]
    [Range(0.1f, 10f)]
    public float m_movement_base_speed = 1.0f;
    public bool m_isAttacking = false;

    [Space]
    [Header("Character statistics:")]
    public float m_movementSpeed = 1f;
    public Vector2 m_movementDirection;

    [Space]
    [Header("References:")]
    public Rigidbody2D m_rb;
    public Animator m_animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_isAttacking = Input.GetButtonDown("Fire1");

        ProcessInputs();
        Move();
        Animate();
    }

    void ProcessInputs() 
    {
        m_movementDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        m_movementSpeed = Mathf.Clamp(m_movementDirection.magnitude, 0.0f, 1.0f);
        m_movementDirection.Normalize();
    }

    void Move()
    {
        m_rb.velocity = m_movementDirection * m_movementSpeed * m_movement_base_speed;
    }

    void Animate()
    {
        if (m_movementDirection != Vector2.zero) {
            m_animator.SetFloat("Horizontal", m_movementDirection.x);
            m_animator.SetFloat("Vertical", m_movementDirection.y);
        }
        m_animator.SetFloat("Speed", m_movementSpeed);
        if (m_isAttacking)
            m_animator.SetFloat("isAttacking", 1.0f);
        else
            m_animator.SetFloat("isAttacking", 0f);
    }
}
