﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator animator;

    private string[] idleDirections = {"idle_N", "idle_NW", "idle_W", "idle_SW", "idle_S", "idle_SE", "idle_E", "idle_NE"};
    private string[] runDirections = {"run_N", "run_NW", "run_W", "run_SW", "run_S", "run_SE", "run_E", "run_NE"};
    public int lastDirection;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDirection(Vector2 direction)
    {
        string[] directionArray = null;

        if (direction.magnitude < 0.01)
        {
            directionArray = idleDirections;
        }
        else
        {
            directionArray = runDirections;

            lastDirection = DirectionToIndex(direction);
            animator.SetInteger("direction", lastDirection);
        }

        // Debug.Log("animating: " + directionArray[lastDirection]);
        // animator.Play(directionArray[lastDirection]);
        animator.SetTrigger(directionArray[lastDirection]);
    }

    private int DirectionToIndex(Vector2 direction)
    {
        Vector2 normalizedDirection = direction.normalized;
        float step = 360/8;
        float offset = step / 2;

        float angle = Vector2.SignedAngle(Vector2.up, normalizedDirection);

        angle += offset;
        if (angle < 0)
        {
            angle += 360;
        }

        float stepCount = angle / step;
        int animNr = Mathf.FloorToInt(stepCount);
        return Mathf.FloorToInt(stepCount);
    }
}
