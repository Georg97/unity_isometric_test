﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform player;
    public float maxSize = 50f;
    public float minSize = 0.1f;
    public float sizeChangeScale = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x, player.position.y, player.position.z - 1);

        if (Input.mouseScrollDelta.y > 0.1f || Input.mouseScrollDelta.y < -0.1f)
            setCameraSize((Input.mouseScrollDelta.y * -1) * sizeChangeScale);
    }

    void setCameraSize(float deltaSize)
    {
        float newSize = GetComponent<Camera>().orthographicSize + deltaSize;
        if (newSize > maxSize)
            newSize = maxSize;
        else if (newSize < minSize)
            newSize = minSize;
        GetComponent<Camera>().orthographicSize = newSize;
    }
}
