﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Animator m_animator;
    public PlayerAnimation m_playerAnimationScript;

    public string[] m_attackDirections = {"attack_N", "attack_NW", "attack_W", "attack_SW", "attack_S", "attack_SE", "attack_E", "attack_NE"};
    public float attackRate = 2f;
    float timeToNextAttack = 0f;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_playerAnimationScript = GetComponent<PlayerAnimation>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= timeToNextAttack)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("space bar pressed, playing attack animation");
                Attack();
                timeToNextAttack = Time.time + 1f / attackRate;
            }
        }
    }

    void Attack()
    {
        /* TODO: play attack animation */
        int animationDirection = m_playerAnimationScript.lastDirection;
        // m_animator.Play(m_attackDirections[animationDirection]);
        m_animator.SetTrigger(m_attackDirections[animationDirection]);
        /* TODO: get enemy objects */
        /* TODO: damage enemies */
    }
}
