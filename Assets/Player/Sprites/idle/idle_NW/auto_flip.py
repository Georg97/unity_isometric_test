import cv2

origin = cv2.imread("idle_NW_001.png")
flipped = cv2.flip(origin, 0)

cv2.imshow("origin", origin)
cv2.imshow("flipped", flipped)

cv2.waitKey()
cv2.destroyAllWindows()