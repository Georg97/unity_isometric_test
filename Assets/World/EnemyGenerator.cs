﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject enemy;
    public Vector2 xVector;
    public Vector2 yVector;
    private float xPos;
    private float yPos;

    public float spawnIntervall = 10f;
    private float nextSpawnTime = 0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnEnemy();
            nextSpawnTime = Time.time + spawnIntervall;
        }
    }

    void SpawnEnemy()
    {
        xPos = Random.Range(xVector.x, xVector.y);
        yPos = Random.Range(yVector.x, yVector.y);
        Instantiate(enemy, new Vector3(xPos, yPos, 0f), Quaternion.identity);
    }
}
